import {Injectable} from '@angular/core';
import MovieOverview from '../type/movie-overview';
import {Observable, of} from 'rxjs';
import {delay} from 'rxjs/operators';
import {getRandomInt} from './random';

@Injectable({
  providedIn: 'root'
})
export class MovieListService {

  // Questo simula il nostro database!
  private readonly movies: MovieOverview[] = [
    {
      id: 1,
      title: 'Joker',
      year: 2019,
      cover: 'https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SY1000_CR0,0,674,1000_AL_.jpg'
    },
    {
      id: 2,
      title: 'The Dictator',
      year: 2012,
      cover: 'https://m.media-amazon.com/images/M/MV5BNTlkOWYzZDYtNzQ1MS00YTNkLTkyYTItMjBmNjgyMDBlMjI4XkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SY1000_CR0,0,675,1000_AL_.jpg'
    },
    {
      id: 3,
      title: 'Titanic',
      year: 1997,
      cover: 'https://m.media-amazon.com/images/M/MV5BMDdmZGU3NDQtY2E5My00ZTliLWIzOTUtMTY4ZGI1YjdiNjk3XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SY1000_CR0,0,671,1000_AL_.jpg'
    },
    {
      id: 4,
      title: 'Back To The Future',
      year: 1985,
      cover: 'https://m.media-amazon.com/images/M/MV5BZmU0M2Y1OGUtZjIxNi00ZjBkLTg1MjgtOWIyNThiZWIwYjRiXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SY1000_CR0,0,643,1000_AL_.jpg'
    },
    {
      id: 5,
      title: 'Harry Potter and the Sorcerer\'s Stone',
      year: 2001,
      cover: 'https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_.jpg'
    }
  ];

  constructor() {
  }

  /**
   * Ritorna la lista di tutti i film.
   */
  public getMovies(): Observable<MovieOverview[]> {
    return of([...this.movies]).pipe(
      delay(getRandomInt(100, 2000))
    );
  }

  /**
   * Ritorna la lista dei film filtrati per titolo.
   * @param title il titolo da cercare.
   */
  public searchMovies(title: string): Observable<MovieOverview[]> {
    const filteredMovies = this.movies.filter(m => m.title.toLowerCase().indexOf(title.toLowerCase()) > -1);
    return of(filteredMovies).pipe(
      delay(getRandomInt(100, 2000))
    );
  }

}
