import {Injectable} from '@angular/core';
import MovieDetails from '../type/movie-details';
import {Observable, of} from 'rxjs';
import {delay} from 'rxjs/operators';
import {getRandomInt} from './random';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailService {

  // Questo simula il nostro database!
  private readonly details: MovieDetails[] = [
    {
      id: 1,
      actors: ['Joaquin Phoenix', 'Robert De Niro', 'Zazie Beetz'],
      description: 'In Gotham City, mentally-troubled comedian Arthur Fleck is disregarded and mistreated by society. He then embarks on a downward spiral of revolution and bloody crime. This path brings him face-to-face with his alter-ego: "The Joker".',
      genres: ['Crime', 'Drama', 'Thriller']
    },
    {
      id: 2,
      actors: ['Sacha Baron Cohen', 'Anna Faris', 'John C. Reilly'],
      description: 'The heroic story of a dictator who risked his life to ensure that democracy would never come to the country he so lovingly oppressed.',
      genres: ['Comedy']
    },
    {
      id: 3,
      actors: ['Leonardo DiCaprio', 'Kate Winslet'],
      description: 'A seventeen-year-old aristocrat falls in love with a kind but poor artist aboard the luxurious, ill-fated R.M.S. Titanic.',
      genres: ['Drama', 'Romance']
    },
    {
      id: 4,
      actors: ['Michael J. Fox', 'Christopher Lloyd', 'Lea Thompson'],
      description: 'Marty McFly, a 17-year-old high school student, is accidentally sent thirty years into the past in a time-traveling DeLorean invented by his close friend, the maverick scientist Doc Brown.',
      genres: ['Adventure', 'Comedy', 'Sci-Fi']
    },
    {
      id: 5,
      actors: ['Daniel Radcliffe', 'Rupert Grint', 'Emma Watson'],
      description: 'An orphaned boy enrolls in a school of wizardry, where he learns the truth about himself, his family and the terrible evil that haunts the magical world.',
      genres: ['Adventure', 'Family', 'Fantasy']
    }
  ];

  constructor() {
  }

  /**
   * Ritorna i dettagli di un film se esiste, undefined altrimenti.
   * @param id id del film di cui si cercano i dettagli.
   */
  public getMovieDetail(id: number): Observable<MovieDetails> {
    const movieDetail = this.details.filter(d => d.id === id).shift();
    return of(movieDetail).pipe(
      delay(getRandomInt(100, 2000))
    );
  }
}
