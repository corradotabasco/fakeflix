import { TestBed } from '@angular/core/testing';

import { MovieListService } from './movie-list.service';

describe('MovieListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MovieListService = TestBed.get(MovieListService);
    expect(service).toBeTruthy();
  });

  it('should return 5 movies', async () => {
    const service: MovieListService = TestBed.get(MovieListService);
    const movies = await service.getMovies().toPromise();
    expect(movies.length).toBe(5);
  });

  it('should return Titanic', async () => {
    const service: MovieListService = TestBed.get(MovieListService);
    const movies = await service.searchMovies('titanic').toPromise();
    expect(movies.length).toBe(1);
  });

  it('should should return empty', async () => {
    const service: MovieListService = TestBed.get(MovieListService);
    const movies = await service.searchMovies('avengers').toPromise();
    expect(movies.length).toBe(0);
  });

});
