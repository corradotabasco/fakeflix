import { TestBed } from '@angular/core/testing';

import { MovieDetailService } from './movie-detail.service';

describe('MovieDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MovieDetailService = TestBed.get(MovieDetailService);
    expect(service).toBeTruthy();
  });

  it('should find details', async () => {
    const service: MovieDetailService = TestBed.get(MovieDetailService);
    const detail = await service.getMovieDetail(1).toPromise();
    expect(detail).toBeTruthy();
  });

  it('should not find details', async () => {
    const service: MovieDetailService = TestBed.get(MovieDetailService);
    const detail = await service.getMovieDetail(1000).toPromise();
    expect(detail).toBeUndefined();
  });

});
