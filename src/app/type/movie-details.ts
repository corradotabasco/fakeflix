export default interface MovieDetails {
  id: number;
  genres: string[];
  description: string;
  actors: string[];
}
