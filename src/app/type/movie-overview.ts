export default interface MovieOverview {
  id: number;
  title: string;
  year: number;
  cover: string;
}
