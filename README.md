# FakeFlix

Obiettivo della traccia è realizzare un'applicazione master/detail che permetta di recuperare una lista di film da un servizio e visualizzarli in una lista. Al click di uno degli elementi della lista, visualizzare i dettagli del film in oggetto.

Obiettivo bonus: implementare una ricerca per titolo. 

## How To
L'applicazione sarà composta da 3 components:
- app: la root della nostra applicazione (il componente è già presente);
- movie-list: la vista con lista dei film;
- movie-detail: la vista con i dettagli di uno specifico film.

I components movie-list e movie-details **NON** dovranno accedere ai servizi: la root sarà responsabile di iniettare i dati corretti all'interno di questi componenti.

## Servizi
Per recuperare i film, ci sono due servizi a disposizione:
- movie-list
- movie-detail


